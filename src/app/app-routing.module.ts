import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ListingComponent} from "./components/listing/listing.component";
import {AddCourseComponent} from "./components/add-course/add-course.component";
import {DetailComponent} from "./components/detail/detail.component";
import {EditComponent} from "./components/edit/edit.component";

const routes: Routes = [
  {path: '', component: ListingComponent},
  {path: 'add', component: AddCourseComponent},
  {path: ':id', component: DetailComponent},
  {path: 'edit/:id', component: EditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
