import { Injectable } from '@angular/core';
import {Article} from "../models/article";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  listeCourse: Article[] = [];

  constructor() { }

  add(course: Article): void {
    course.id = Date.now();
    this.listeCourse.push(course);
  }

  findById(id: number): Article {
    return this.listeCourse.filter(course => course.id == id)[0];
  }

  remove(id: number): void {
    this.listeCourse = this.listeCourse.filter(course => course.id != id);
  }

  update(course: Article): void {
    if(course.id){
      let oldCourse: Article = this.findById(course.id);
      oldCourse.label = course.label;
    }
  }
}
