import { Component, OnInit } from '@angular/core';
import {ArticleService} from "../../services/article.service";
import {Article} from "../../models/article";

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {
  listeCourse: Article[] = [];
  constructor(private serviceArtcile: ArticleService) { }

  ngOnInit(): void {
    this.listeCourse = this.serviceArtcile.listeCourse;
  }

  deleteArticle(id: number){
    this.serviceArtcile.remove(id);
    this.listeCourse = this.serviceArtcile.listeCourse;
  }

}
