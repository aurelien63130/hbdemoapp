import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ArticleService} from "../../services/article.service";
import {Article} from "../../models/article";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  articleEditForm !: Article;

  constructor(private activatedRoute: ActivatedRoute,
              private articleService: ArticleService,
              private router: Router) { }

  ngOnInit(): void {
    let id: number = parseInt(<string>this.activatedRoute.snapshot.paramMap.get('id'));
    this.articleEditForm = this.articleService.findById(id);
  }

  submitEditForm(): void {
    this.articleService.update(this.articleEditForm);
    this.router.navigate(["/"]);
  }

}
