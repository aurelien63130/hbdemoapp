import { Component, OnInit } from '@angular/core';
import {Article} from "../../models/article";
import {Router} from "@angular/router";
import {ArticleService} from "../../services/article.service";

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css']
})
export class AddCourseComponent implements OnInit {
  courseForm: Article = new Article();

  constructor(private router: Router, private articleService: ArticleService) { }

  ngOnInit(): void {
  }

  submitForm(): void {
    this.articleService.add(this.courseForm);
    this.router.navigate(["/"]);
  }

}
